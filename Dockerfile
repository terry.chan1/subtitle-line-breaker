FROM python:3.10-slim

WORKDIR /app

RUN apt-get update; apt-get install -y git; apt-get -y install curl

COPY requirements.txt ./
RUN pip3 install --upgrade pip setuptools wheel
RUN pip3 install -r requirements.txt

COPY src /app

EXPOSE 8080
CMD [ "python", "subtitle_line_breaker.py" ]