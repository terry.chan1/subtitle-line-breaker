import sys, logging
from itertools import chain
from flask import Flask, request, make_response, render_template_string
from flask_restx import Api, Resource, fields

from deephow.internal.language_toolkit.tokenize import sentence_tokenize
from deephow.internal.language_toolkit.tokenize import word_tokenize

logging.basicConfig(
    filename = 'logging.log',
    filemode = 'w',
    level = logging.DEBUG,
    format = '%(asctime)s %(name)s [%(levelname)s] %(message)s',
    datefmt = '%Y-%m-%d %H:%M:%S'
)

app = Flask(__name__)
api = Api(app, doc='/api/', version="1.0.3")

post_payload = api.model('edit video payload', {
    'sentences': fields.List(fields.String, required=True, example=[
        "DeepHow bridges the skills gap in manufacturing, service and repair through an AI-powered learning platform based on interactive how-to videos.",
        "This is DeepHow the first AI solution for learning and training skilled trades, while experts perform their individual tasks and processes. DeepHow captures their workflows via a mobile device using the latest AI technology."
        ]), 
    'language': fields.String(required=False, example="en-us"), 
    'token': fields.String(required=False, example="")
})

# get service parameters
try:
    from deephow.internal.admin.database.database_service import DatabaseService
    db = DatabaseService() #default/cloudrun deploy ver
    _, db_document = db.instance().get_document(collection="configurations", document_id='language')
    db_config = db_document.get('subtitleLineBreaker', {})
    AUTH_TOKEN = str(db_config.get('token', '0d65ec1e80aa8e2fefeacc3412b5143b'))
    BREAK_RATIO = float(db_config.get('break_ratio', 0.5))
    db_max_lengths = db_document.get('timedTextStyleGuide', {})
    MAX_LENGTH = {k: v['characterLimitation'] for k, v in db_max_lengths.items()}
except ImportError: #local ver
    logging.info('import deephow admin error, use default setting')
    AUTH_TOKEN = '0d65ec1e80aa8e2fefeacc3412b5143b'
    BREAK_RATIO = 0.5
    # define break thresholds for each language (refer to Netfilx)
    max_len1 = dict.fromkeys(['zh', 'zh-tw', 'ko-kr'], 16)
    MAX_LENGTH = {**max_len1, 'ru-ru':39, 'th-th':35, 'ja-jp':13, 'en-us': 45, 'default': 45}

@api.route('/', methods=['POST'])
@api.expect(post_payload)
class SubtitleLineBreaker(Resource):
    def post(self):
        if not 0 <= BREAK_RATIO <= 1:
            return 'invalid break_ratio, must be between 0 and 1', 400

        # parse payload content
        try:
            content = request.json
            logging.debug(f'received: {content}')
        except ValueError:
            return 'invalid JSON', 400

        if content.get('token') != AUTH_TOKEN:
            return 'invalid token', 400               

        language, sentences = str(content.get('language', 'en-us')).lower(), content.get('sentences', [])
        max_sentence_length = MAX_LENGTH.get(language, MAX_LENGTH['default'])

        # start line breaking
        ret = []
        # print(sentences)
        for text in sentences:
            # print(len(text))
            # no need to break
            if len(text) <= max_sentence_length:
                ret.append(text)
                continue

            expect_break = len(text) * BREAK_RATIO
            accumulate = 0 #record current position in text
            special_cases = ['ar-ar', 'ja-jp', 'th-th', 'zh', 'zh-tw']
            if language in special_cases:
                # merge words in sentences into a single word list
                # punctuation mark removed by sentence_tokenize() is replaced with a ' ' to preserve correct sentence length
                words = list(chain.from_iterable((word_tokenize(s, language) + [' '] for s in sentence_tokenize(text, language))))
            else:
                words = [w + ' ' for w in text.split(' ')] #restore the sapce after each word to preserve correct sentence length
            # print(expect_break, words)
            for word in words:
                prev_acc = accumulate
                accumulate += len(word)
                # print(accumulate)
                if accumulate > expect_break: #find break point!
                    # print('break!', end=' ')
                    index = accumulate if abs(expect_break-accumulate) < abs(expect_break-prev_acc) else prev_acc
                    # print(index)
                    ret.append(text[:index] + '\r\n' + text[index:])
                    break            
        logging.debug(f'sentences: {ret}')
        return {'sentences': ret, 'language': language, 'break_ratio': BREAK_RATIO}, 200

@api.route('/test', methods=['GET'])
class Test(Resource):
    def get(self):
        return make_response(render_template_string('''
        <!DOCTYPE html>
        <html>
        <head>
            <link rel="shortcut icon" href="#"/> <!--prevent "GET /favicon.ico HTTP/1.1" 404-->
            <meta charset="utf-8">
            <title>test page</title>
        </head>
        <body>
            <form id="login-form">
                sentence1:<input type="text" name="sentence1" size="90" class="form-control"/><br>
                sentence2:<input type="text" name="sentence2" size="90" class="form-control"/><br>
                language:
                <select name="language">
                    <option value="en-us">en-us(45)</option>
                    <option value="zh">zh(16)</option>
                    <option value="ja-jp">ja-jp(13)</option>
                    <option value="ru-ru">ru-ru(39)</option>
                    <option value="th-th">th-th(35)</option>
                </select><br>
                <!--break_ratio:<input type="number" step="any" name="break_ratio" min="0" max="1" value="0.5"/><br>-->
                token:<input type="text" name="token" class="form-control" /><br>
                <input type="submit" class="btn btn-danger" value="Submit" class="form-control"/>
            </form>
            <p id="message0">{{ url_for('subtitle_line_breaker', _external=True) }}</p>
            <hr><p id="message1"></p>
            <hr><p id="message2"></p>
            <script>
            //get the form from DOM (Document object model) 
            var form = document.getElementById('login-form');
            form.onsubmit = function(event){               
                var xhr = new XMLHttpRequest();
                var data = new FormData(form);
                var data2 = JSON.stringify({
                    sentences: [data.get('sentence1'), data.get('sentence2')],
                    language: data.get('language'),
                    //break_ratio: data.get('break_ratio'),
                    token: data.get('token')
                });
                
                //open the request
                var url = "{{ url_for('subtitle_line_breaker', _external=True) }}"
                xhr.open('POST', url);
                xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                xhr.setRequestHeader("Access-Control-Allow-Origin", "*");
                xhr.setRequestHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS");                
                //xhr.setRequestHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
                //send the form data
                xhr.send(data2);
                
                xhr.onreadystatechange = function() {
                    if (xhr.readyState === 4 && xhr.status === 200) {
                        var ret = JSON.parse(xhr.responseText);
                        document.getElementById("message1").innerHTML = "<pre>" + ret.sentences[0] + "</pre>"
                        document.getElementById("message2").innerHTML = "<pre>" + ret.sentences[1] + "</pre>"
                        document.getElementById("message0").innerHTML = "send to: " + url
                    }
                }
                //Dont submit the form.
                return false; 
            }
            </script>
        </body>
        </html>
        '''))

@api.route('/log', methods=['GET'])
class Log(Resource):
    def get(self):
        with open('logging.log', 'r') as f:
            response = make_response(f.read(), 200)
            response.mimetype = "text/plain"
            return response

if __name__ == '__main__':
    # text = "This is DeepHow the first AI solution for learning and training skilled trades, while experts perform their individual tasks and processes. DeepHow captures their workflows via a mobile device using the latest AI technology."
    # print(sentence_tokenize(text, 'en-us'))

    # text = "今天中國經濟已經由高速發展轉入高質量發展的軌道，高質量的發展就需要高質量的技術人才。然而，生產製造業仍然面臨技術人才大量短缺的難題。 當上一代資深專家即將退休，如何將他們的專業之匙行業技能和工作經驗傳授給年輕一代？"
    # print(sentence_tokenize(text, 'zh-tw'))

    # text = "DeepHow bridges the skills gap in manufacturing, service and repair through an AI-powered learning platform based on interactive how-to videos."
    # print(word_tokenize(text, 'en-us'))

    # text = "我们希望将以人为本的AI带入制造，服务和营建领域，以协助客户培训优秀的未来劳动力。"
    # print(word_tokenize(text, 'zh'))

    host = str(sys.argv[1]) if len(sys.argv) > 1 else '0.0.0.0'
    port = int(sys.argv[2]) if len(sys.argv) > 2 else 8080
    app.run(host=host, port=port, debug=True)
