# Subtitle Line Breaker
Python library as a toolkit for splitting subtitle sentences.
<p align="left">
    <a><img src="https://img.shields.io/badge/version-v1.0.3-brightgreen" /></a>
    <a><img src="https://img.shields.io/badge/python-3.6%20%7C%203.7%20%7C%203.8%20%7C%203.9%20%7C%203.10-blue" /></a>
    <a><img src="https://img.shields.io/badge/platform-linux%20%7C%20macos%20%7C%20windows-lightgrey" /></a>
</p>

## Installation
```
pip3 install -r requirements.txt
```
This project uses DeepHow [python-deephow-admin](https://gitlab.com/deephowlibs/python-deephow-admin) and [python-language-toolkit](https://gitlab.com/deephowlibs/python-language-toolkit).

## Usage
run by
```
python3 subtitle_line_breaker.py
```
or
```
python3 subtitle_line_breaker.py {your IP} {your port}
```

POST to http://your-IP/ with json
```
{
  "sentences": ["sentence1", "sentence2", ...],
  "language": supported language code,
  "token": "0d65ec1e80aa8e2fefeacc3412b5143b"
}
```
For testing:
* test webpage: http://your-IP/test
* swagger API page: http://your-IP/api

Supported language code examples

| Language            | Internal Language Code |
|---------------------|------------------------|
| Arabic              | ar-ar                  |
| Czech               | cs-cz                  |
| German              | de-de                  |
| English             | en-us                  |
| Spanish             | es-mx                  |
| French              | fr-fr                  |
| Hebrew              | he-il                  |
| Hungarian           | hu-hu                  |
| Italian             | it-it                  |
| Japanese            | ja-jp                  |
| Korean              | ko-kr                  |
| Dutch               | nl-nl                  |
| Polish              | pl-pl                  |
| Portuguese          | pt-br                  |
| Romanian            | ro-ro                  |
| Russian             | ru-ru                  |
| Thai                | th-th                  |
| Vietnamese          | vi-vn                  |
| Simplified Chinese  | zh                     |
| Traditional Chinese | zh-tw                  |

## Author
terry.chan@deephow.com